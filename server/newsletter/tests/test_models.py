from django.test import TestCase
from newsletter.models import Newsletter


class TestNewsletterModel(TestCase):
    def setUp(self):
        self.email = 'calinvladth@icloud.com'
        self.obj = Newsletter.objects.create(email=self.email)

    def test_get(self):
        obj = Newsletter.objects.get(email=self.email)
        self.assertEqual(self.email, obj.email)

    def test_get_all(self):
        obj = Newsletter.objects.all()
        self.assertTrue(obj.count() > 0)

    def test_create(self):
        obj = Newsletter.objects.create(email='test@email.com')
        self.assertTrue(obj.pk)

    def test_delete(self):
        obj = Newsletter.objects.delete(pk=self.obj.pk)
        self.assertFalse(obj.id)
