from django.db import models
from globals.time_stamp import TimeStamp
from newsletter.managers import NewsletterManager


class Newsletter(TimeStamp):
    email = models.EmailField(unique=True)

    objects = NewsletterManager()
