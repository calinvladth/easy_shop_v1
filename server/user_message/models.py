from django.db import models

from globals.time_stamp import TimeStamp
from user_message.managers import UserMessageManager


class UserMessage(TimeStamp):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    subject = models.CharField(max_length=250)
    message = models.TextField()

    objects = UserMessageManager()