from django.db import models


class UserMessageManager(models.Manager):
    def get(self, **kwargs):
        obj = self.filter(**kwargs).first()
        if not obj:
            raise ValueError('User message does not exist')
        return obj

    def create(self, **kwargs):
        obj = self.model(**kwargs)
        obj.save()
        return obj

    def delete(self, pk):
        obj = self.model.objects.get(pk=pk)
        obj.delete()
        return obj
