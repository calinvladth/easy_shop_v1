base_data = [
    {"name": "product 1", "price": 20},
    {"name": "product 2", "price": 40},
    {"name": "product 3", "price": 30}
]

path_1 = 'image_1.png'
path_2 = 'image_2.png'

images = [
    {'path': path_1},
    {'path': path_2}
]

specs = [
    {'key': 'weight', 'value': '150gr'},
    {'key': 'height', 'value': '15cm'}
]
