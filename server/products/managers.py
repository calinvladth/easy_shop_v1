import time
from django.db import models


class ProductManager(models.Manager):
    def get_all(self, **kwargs):
        obj = self.filter(**kwargs).order_by('created')
        return reversed(list(obj))

    def get(self, **kwargs):
        obj = self.filter(**kwargs).first()
        if not obj:
            raise ValueError('Product does not exist')
        return obj

    def create(self, **kwargs):
        if 'name' not in kwargs or not kwargs['name']:
            raise ValueError('Product name is required')

        obj = self.model(**kwargs)
        obj.save()
        return obj

    def edit(self, pk, **kwargs):
        obj = self.model.objects.get(pk=pk)
        for (key, value) in kwargs.items():
            setattr(obj, key, value)
        obj.modified = time.time()
        obj.save()
        return obj

    def delete(self, pk):
        obj = self.model.objects.get(pk=pk)
        obj.delete()
        return obj


class ImageManager(models.Manager):

    def get(self, pk):
        obj = self.filter(id=pk).first()
        if not obj:
            raise ValueError('Image not found')
        return obj

    def create(self, **kwargs):
        self.model.objects.check_extension(image=kwargs['path'])
        obj = self.model(**kwargs)
        obj.save()
        return obj

    def bulk_edit_index(self, **kwargs):
        for o in kwargs['images']:
            obj = self.model.objects.get(pk=o['id'])
            obj.index = o['index']
            obj.save()

        objs = self.model.objects.filter(product=kwargs['product'])

        return objs

    @staticmethod
    def check_extension(image):
        obj = str(image).lower()
        approved_extensions = ['.png', '.jpeg', '.jpg']
        match = False
        extension = ''

        for o in approved_extensions:
            if obj.endswith(o):
                match = True
                extension = o
                break

        if not match:
            raise ValueError('File format is invalid')

        return extension


class SpecManager(models.Manager):

    def get(self, pk):
        obj = self.filter(id=pk).first()
        if not obj:
            raise ValueError('Spec not found')
        return obj

    def create(self, **kwargs):
        obj = self.model(**kwargs)
        obj.save()
        return obj

    def edit(self, pk, **kwargs):
        obj = self.model.objects.get(pk=pk)

        for (key, value) in kwargs.items():
            setattr(obj, key, value)

        obj.save()
        return obj

    def bulk_edit_index(self, **kwargs):
        for o in kwargs['specs']:
            obj = self.model.objects.get(pk=o['id'])
            obj.index = o['index']
            obj.save()

        objs = self.model.objects.filter(product=kwargs['product'])

        return objs

