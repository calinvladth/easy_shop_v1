from rest_framework.views import APIView
from rest_framework import status

from globals.response import custom_response
from accounts.models import Account


class CheckServerStatus(APIView):
    def get(self, request):
        return custom_response(
            status=status.HTTP_200_OK,
            message='Server is on'
        )


class CheckRegister(APIView):
    def get(self, request):
        obj = Account.objects.filter(is_admin=True)
        allow = obj.count() < 1

        return custom_response(
            status=status.HTTP_200_OK,
            message=f'{"ALLOWED" if allow else "NOT ALLOWED"}',
            data={
                'register': allow
            }
        )
