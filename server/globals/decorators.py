from accounts.models import Account


def account_is_admin(pk):
    account = Account.objects.get(id=pk)
    if not account.is_staff:
        raise PermissionError('You can\'t perform this action')

    return account
