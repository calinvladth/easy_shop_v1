import React from 'react'
import style from './index.module.sass'
import {Link, useLocation} from "react-router-dom";
import {ProductsPath} from "../../pages/products";
import {OrdersPath} from "../../pages/orders";
import {useDispatch} from "react-redux";
import {LogOut} from "../../redux/authentication/actions";
import {ConfigPath} from "../../pages/config";
import {MessagesPath} from "../../pages/mesages";
import {EmailsPath} from "../../pages/emails";

const HeaderComponent = () => {
    const location = useLocation()
    const dispatch = useDispatch()
    return (
        <div className={style.box}>
            <div className={style.logOut}>
                <span onClick={() => dispatch(LogOut())}>Logout</span>
            </div>
            <div className={style.links}>
                <div className={location.pathname === ProductsPath ? style.active : null}>
                    <Link to={ProductsPath}>Products</Link>
                </div>
                <div className={location.pathname === OrdersPath ? style.active : null}>
                    <Link to={OrdersPath}>Orders</Link>
                </div>

                <div className={location.pathname === MessagesPath ? style.active : null}>
                    <Link to={MessagesPath}>Messages</Link>
                </div>

                <div className={location.pathname === EmailsPath ? style.active : null}>
                    <Link to={EmailsPath}>Emails</Link>
                </div>

                <div className={location.pathname === ConfigPath ? style.active : null}>
                    <Link to={ConfigPath}>Config</Link>
                </div>
                <div>
                    <a href={process.env.REACT_APP_STORE_URL}>Store</a>
                </div>
            </div>


        </div>
    )
}

export default HeaderComponent